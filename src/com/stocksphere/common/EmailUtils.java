package com.stocksphere.common;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class EmailUtils {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(EmailUtils.class);
	
	public void sendEmailWithAttachment(String fileNameWithPath,String fileName){
		
		log.info("Sending Email");
		
		final String username = AppConstants.EMAIL_SENDER_USERNAME;
		final String password = AppConstants.EMAIL_SENDER_PASSWORD;
		final String host = "smtp.gmail.com";
		Transport transport = null;
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		
		try {
			
			Date date = new DateTime(new Date()).withZone(DateTimeZone.forID( "Asia/Kolkata" )).toDate();
 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username, AppConstants.EMAIL_SENDER_NAME));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(AppConstants.RECEIPENTS_EMAIL));
			message.setSubject("Stock Details");
			
			//For attachment
			// Create the message part 
	         BodyPart messageBodyPart = new MimeBodyPart();

	         // Fill the message
	         messageBodyPart.setText(" Hi,"
	 				+ "\n\n Please find stock details for date-time: "+date
	 			+ "\n\n Cheers!"
	 			+ "\n Stock Sphere Team"
	 			+ "\n\n *** This is an automatically generated email, but you can reply. We'll appreciate your response :) ***");
	         
	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment
	         messageBodyPart = new MimeBodyPart();
	        
	         DataSource source = new FileDataSource(fileNameWithPath);
	         
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(fileName);
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart,"application/vnd.ms-excel");

	         // set the message content here
	        transport = session.getTransport("smtps");
			transport.connect(host, username, password);
			transport.sendMessage(message, message.getAllRecipients());
 
			log.info("Email successfully sent");
 
		} catch (MessagingException e) {
			
			log.error("MessagingException while sending email");
			log.error(e);
		}
		catch (Exception e) {
			log.error("Exception while sending email");
			log.error(e);
		}
		finally
		{
			if(transport != null)
			{
				try 
				{
					transport.close();
				} catch (MessagingException e)
				{
					log.error("MessagingException while closing transport connection");
					log.error(e);
				}
			}
		}
	}
}
