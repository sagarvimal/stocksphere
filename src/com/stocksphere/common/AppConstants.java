package com.stocksphere.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AppConstants {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(AppConstants.class);
	
	public static final String DB_NAME = "db_stock";
	
	public static final String DB_COLLECTION_STOCK_NEWS = "db_col_stock_news";
	
	public static final String DB_COLLECTION_STOCK_QUOTE_NSE = "db_col_stock_quote_nse";
	
	public static final String NEWS_URL = "url";
	
	public static final String SCRIPT_NAME = "script_name";
	
	public static final String STOCK_NEWS_PORTAL_MONEYCONTROL = "moneycontrol";
	
	public static final String STOCK_NEWS_PORTAL_ET = "economic_times";
	
	public static final String BOT_NAME = "SS_BOT";
	
	public static final String MONEYCONTROL_DATE_FORMAT = "MMM dd, yyyy, hh.mm a Z";
	
	public static final String ET_DATE_FORMAT = "dd MMM, yyyy, hh.mma Z";
	
	public static final String COMPARISION_PARAMETER_PERCENTAGE = "PRECENTAGE";
	
	public static final String COMPARISION_PARAMETER_DATE_PUBLISHED = "DATE_PUBLISHED";
	
	public static final String SPACE = " ";
	public static final String DOTS = "...";
	
	public static final String CATEGORY_ALL = "CAT_ALL";
	public static final String CATEGORY_1 = "CAT_1";
	public static final String CATEGORY_2 = "CAT_2";
	public static final String CATEGORY_3 = "CAT_3";
	public static final String CATEGORY_4 = "CAT_4";
	public static final String CATEGORY_5 = "CAT_5";
	
	public static final String DOMAIN_MONEYCONTROL = "moneycontrol.com";
	public static final String DOMAIN_ET = "indiatimes.com";
	
	public static final int STANDARD_HOURS = 100;
	
	
	//***************************************************************
	// Properties file data
	
	public static String EXECUTION_TYPE;
	
	public static String EMAIL_SENDER_USERNAME;
	public static String EMAIL_SENDER_PASSWORD;
	public static String EMAIL_SENDER_NAME;
	public static String RECEIPENTS_EMAIL;
	public static String EXCEL_FILE_NAME;
	
	public static String NEWS_CRAWL_STORAGE_FOLDER;
	public static String QUOTE_CRAWL_STORAGE_FOLDER;
	public static String NEWS_NUMBER_OF_CRAWLERS;
	public static String NEWS_DEPTH_OF_CRAWLING;
	public static String QUOTE_NUMBER_OF_CRAWLERS;
	public static String QUOTE_DEPTH_OF_CRAWLING;
	public static String CRAWLER_USER_AGENT;
	
	public static String MONGO_DB_CONNECTION;
	public static String USER_PATH;
	
	static
	{
		try 
		{
			log.info("Reading properties file");

			// get user path
			AppConstants.USER_PATH=System.getProperty("user.dir")+"/";

			File file = new File(AppConstants.USER_PATH+"config/config.properties");
			FileInputStream fileInput = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(fileInput);
			fileInput.close();

			Enumeration<Object> enuKeys = properties.keys();
			while (enuKeys.hasMoreElements())
			{
				String key = (String) enuKeys.nextElement();
				String value = properties.getProperty(key);

				if(key != null && !key.isEmpty() && value != null && !value.isEmpty())
				{
					if(key.equalsIgnoreCase("execution.type"))
					{
						AppConstants.EXECUTION_TYPE = value;
						log.info("----- EXECUTION TYPE: "+AppConstants.EXECUTION_TYPE);
					}
					else if(key.equalsIgnoreCase("email.sender.username"))
					{
						AppConstants.EMAIL_SENDER_USERNAME = value;
					}
					else if(key.equalsIgnoreCase("email.sender.password"))
					{
						AppConstants.EMAIL_SENDER_PASSWORD = value;
					}
					else if(key.equalsIgnoreCase("email.sender.name"))
					{
						AppConstants.EMAIL_SENDER_NAME = value;
					}
					else if(key.equalsIgnoreCase("receipents.email"))
					{
						AppConstants.RECEIPENTS_EMAIL = value;
					}
					else if(key.equalsIgnoreCase("excel.file.name"))
					{
						AppConstants.EXCEL_FILE_NAME = value;
					}
					else if(key.equalsIgnoreCase("news.crawl.storage.folder"))
					{
						AppConstants.NEWS_CRAWL_STORAGE_FOLDER = value;
					}
					else if(key.equalsIgnoreCase("quote.crawl.storage.folder"))
					{
						AppConstants.QUOTE_CRAWL_STORAGE_FOLDER = value;
					}
					else if(key.equalsIgnoreCase("news.number.of.crawlers"))
					{
						AppConstants.NEWS_NUMBER_OF_CRAWLERS = value;
					}
					else if(key.equalsIgnoreCase("news.depth.of.crawling"))
					{
						AppConstants.NEWS_DEPTH_OF_CRAWLING = value;
					}
					else if(key.equalsIgnoreCase("quote.number.of.crawlers"))
					{
						AppConstants.QUOTE_NUMBER_OF_CRAWLERS = value;
					}
					else if(key.equalsIgnoreCase("quote.depth.of.crawling"))
					{
						AppConstants.QUOTE_DEPTH_OF_CRAWLING = value;
					}
					else if(key.equalsIgnoreCase("crawler.user.agent"))
					{
						AppConstants.CRAWLER_USER_AGENT = value;
					}
					else if(key.equalsIgnoreCase("mongo.db.connection"))
					{
						AppConstants.MONGO_DB_CONNECTION = value;
					}
					else
					{
						log.info("Look! Extra data in properties file.");
					}

					log.info(key + " : " + value);
				}
				else
				{
					throw new Exception();
				}
			}
			log.info("Successfully read properties file");
		} catch (FileNotFoundException e) 
		{
			log.error("FileNotFoundException while reading properties file");
			log.error(e);
		} catch (IOException e)
		{
			log.error("IOException while reading properties file");
			log.error(e);
		} catch (Exception e)
		{
			log.error("Exception: key or value is null");
			log.error(e);
		}
	}
}
