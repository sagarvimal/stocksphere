package com.stocksphere.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import com.stocksphere.datavalueobject.StockNewsDataVO;
import com.stocksphere.datavalueobject.StockQuoteDataVO;

/*
 * http://poi.apache.org/spreadsheet/quick-guide.html#MergedCells
 */
public class ExcelUtils {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(ExcelUtils.class);

	public void printDataTOExcel(Map<String, List<StockQuoteDataVO>> stockMap,List<StockQuoteDataVO> scriptWithNoNewsList)
	{	
		if(stockMap != null && !stockMap.isEmpty())
		{
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheetAll = null;
			HSSFSheet sheet1 = null;
			HSSFSheet sheet2 = null;
			HSSFSheet sheet3 = null;
			HSSFSheet sheet4 = null;
			HSSFSheet sheet5 = null;
			HSSFSheet sheetNoNews = null;

			List<StockQuoteDataVO> stockQuoteDataVOListAll = stockMap.get(AppConstants.CATEGORY_ALL);
			List<StockQuoteDataVO> stockQuoteDataVOList1 = stockMap.get(AppConstants.CATEGORY_1);
			List<StockQuoteDataVO> stockQuoteDataVOList2 = stockMap.get(AppConstants.CATEGORY_2);
			List<StockQuoteDataVO> stockQuoteDataVOList3 = stockMap.get(AppConstants.CATEGORY_3);
			List<StockQuoteDataVO> stockQuoteDataVOList4 = stockMap.get(AppConstants.CATEGORY_4);
			List<StockQuoteDataVO> stockQuoteDataVOList5 = stockMap.get(AppConstants.CATEGORY_5);

			if(stockQuoteDataVOListAll != null)
			{
				log.info("Printing for All Stocks");
				
				// Create Sheet
				sheetAll = workbook.createSheet("All Stocks");
				this.populateDataInSheet(stockQuoteDataVOListAll, sheetAll,workbook);
			}
			if(stockQuoteDataVOList1 != null)
			{
				log.info("Printing for LMP [<= 20]");
				
				// Create Sheet
				sheet1 = workbook.createSheet("LMP(<20)");
				this.populateDataInSheet(stockQuoteDataVOList1, sheet1,workbook);
			}
			if(stockQuoteDataVOList2 != null)
			{
				log.info("Printing for LMP [>20 - <= 60]");
				
				// Create Sheet
				sheet2 = workbook.createSheet("LMP(20-60)");
				this.populateDataInSheet(stockQuoteDataVOList2, sheet2,workbook);
			}
			if(stockQuoteDataVOList3 != null)
			{
				log.info("Printing for LMP [>60 - <= 150]");
				
				// Create Sheet
				sheet3 = workbook.createSheet("LMP(60-150)");
				this.populateDataInSheet(stockQuoteDataVOList3, sheet3,workbook);
			}
			if(stockQuoteDataVOList4 != null)
			{
				log.info("Printing for LMP (>150 - <= 350)");
				
				// Create Sheet
				sheet4 = workbook.createSheet("LMP(150-350)");
				this.populateDataInSheet(stockQuoteDataVOList4, sheet4,workbook);
			}
			if(stockQuoteDataVOList5 != null)
			{
				log.info("Printing for LMP [>350]");
				
				// Create Sheet
				sheet5 = workbook.createSheet("LMP(>350)");
				this.populateDataInSheet(stockQuoteDataVOList5, sheet5,workbook);
			}
			if(scriptWithNoNewsList !=null && !scriptWithNoNewsList.isEmpty())
			{
				log.info("Printing for Scripts Without News");

				// Create Sheet
				sheetNoNews = workbook.createSheet("Scripts Without News");
				this.populateDataInSheet(scriptWithNoNewsList, sheetNoNews,workbook);
			}
			

			if(sheetAll != null || sheet1 != null || sheet2 != null || sheet3 != null || sheet4 != null || sheet5 != null || sheetNoNews != null)
			{
				try 
				{
					Date tempDate = new Date();
					String fileName = AppConstants.EXCEL_FILE_NAME+"_"+tempDate.toString().replace(" ", "_").replace(":", "_"); 
					fileName = fileName+".xls";
					
					String fileNameWithPath = AppConstants.USER_PATH+"exceldata/"+fileName;
					
					FileOutputStream out = 
							new FileOutputStream(new File(fileNameWithPath));
					workbook.write(out);
					out.close();
					log.info("Excel written successfully. File Name >> "+fileNameWithPath);
					
					new EmailUtils().sendEmailWithAttachment(fileNameWithPath,fileName);

				} catch (FileNotFoundException e) {
					log.error(e);
				} catch (IOException e) {
					log.error(e);
				}
			}
			else
			{
				log.error("No data is avaialbel for print");
			}
		}// End stockMap != null && !stockMap.isEmpty()
	}
	
	private void createHeaderRow(HSSFSheet sheet,HSSFWorkbook workbook)
	{
		CellStyle cellStyle = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(IndexedColors.BLUE.getIndex());
		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		
		//Set filter
		sheet.setAutoFilter(CellRangeAddress.valueOf("A1:K1"));
		
		//Freeze header row
		sheet.createFreezePane(0,1);
		
		//Set column width
		this.setColumnWidth(sheet);
		
		// Create Header row
		Row rowHeader = sheet.createRow((short) 0);
		
		// set rwo height
		rowHeader.setHeightInPoints(4 * sheet.getDefaultRowHeightInPoints());
		
		Cell cell0 = rowHeader.createCell((short) 0);
	    cell0.setCellValue("Script Name");
	    cell0.setCellStyle(cellStyle);
	    
	    Cell cell1 = rowHeader.createCell((short) 1);
	    cell1.setCellValue("Last Market Price");
	    cell1.setCellStyle(cellStyle);
	    
	    Cell cell2 = rowHeader.createCell((short) 2);
	    cell2.setCellValue("Price Change");
	    cell2.setCellStyle(cellStyle);
	    
	    Cell cell3 = rowHeader.createCell((short) 3);
	    cell3.setCellValue("Percentage Change");
	    cell3.setCellStyle(cellStyle);
	    
	    Cell cell4 = rowHeader.createCell((short) 4);
	    cell4.setCellValue("Prvious Close");
	    cell4.setCellStyle(cellStyle);
	    
	    Cell cell5 = rowHeader.createCell((short) 5);
	    cell5.setCellValue("Total News");
	    cell5.setCellStyle(cellStyle);
	    
	    Cell cell6 = rowHeader.createCell((short) 6);
	    cell6.setCellValue("Title");
	    cell6.setCellStyle(cellStyle);
	    
	    Cell cell9 = rowHeader.createCell((short) 7);
	    cell9.setCellValue("Date Published");
	    cell9.setCellStyle(cellStyle);
	    
	    Cell cell7 = rowHeader.createCell((short) 8);
	    cell7.setCellValue("Sub Title");
	    cell7.setCellStyle(cellStyle);
	    
	    Cell cell8 = rowHeader.createCell((short) 9);
	    cell8.setCellValue("Article Summary");
	    cell8.setCellStyle(cellStyle);
	    
	    Cell cell10 = rowHeader.createCell((short) 10);
	    cell10.setCellValue("Portal");
	    cell10.setCellStyle(cellStyle);
	    
	    Cell cell11 = rowHeader.createCell((short) 11);
	    cell11.setCellValue("URL");
	    cell11.setCellStyle(cellStyle);
	}
	
	private void populateDataInSheet(List<StockQuoteDataVO> stockQuoteDataVOList,HSSFSheet sheet,HSSFWorkbook workbook)
	{
		log.info("Populating data in excel");
		try
		{
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setWrapText(true);
		
		this.setColumnWidth(sheet);
		
		List<StockNewsDataVO> stockNewsDataVOList = null;
		StockQuoteDataVO stockQuoteDataVO = null;
		StockNewsDataVO stockNewsDataVO = null;
		
		//Create header row
		this.createHeaderRow(sheet,workbook);
		
	    Row row = null;
	    Cell cellScriptName = null;
	    Cell cellLastMarketPrice = null;
	    Cell cellPriceChange = null;
	    Cell cellPercentageChange = null;
	    Cell cellVolume = null;
	    Cell cellNewsCount = null;
	    Cell cellTitle = null;
	    Cell cellSubTitle = null;
	    Cell cellArticleSummary = null;
	    Cell cellDatePublished = null;
	    Cell cellPortal = null;
	    Cell cellUrl = null;
	    int rowNum = 0;
	    
		Iterator<StockQuoteDataVO> itr = stockQuoteDataVOList.iterator();
		while(itr.hasNext())
		{
			stockQuoteDataVO = itr.next();

			if(stockQuoteDataVO != null)
			{
				stockNewsDataVOList = stockQuoteDataVO.getStockNewsDataVOList();

				if(stockNewsDataVOList != null && !stockNewsDataVOList.isEmpty())
				{
					int size = stockNewsDataVOList.size();

					for(int i=0; i<size; i++)
					{
						stockNewsDataVO = stockNewsDataVOList.get(i);
						row = sheet.createRow(++rowNum);

						cellScriptName = row.createCell((short)0);
						cellScriptName.setCellValue(stockQuoteDataVO.getScriptName());
						cellScriptName.setCellStyle(cellStyle);
						
						cellLastMarketPrice = row.createCell((short)1);
						cellLastMarketPrice.setCellValue(stockQuoteDataVO.getLastMarketPrice());
						cellLastMarketPrice.setCellStyle(cellStyle);
						
						cellPriceChange = row.createCell((short)2);
						cellPriceChange.setCellValue(stockQuoteDataVO.getChangePrice());
						cellPriceChange.setCellStyle(cellStyle);
						
						cellPercentageChange = row.createCell((short)3);
						cellPercentageChange.setCellValue(stockQuoteDataVO.getChangePercentage());
						cellPercentageChange.setCellStyle(cellStyle);
						
						cellVolume = row.createCell((short)4);
						cellVolume.setCellValue(stockQuoteDataVO.getVoulmeTraded());
						cellVolume.setCellStyle(cellStyle);
						
						cellNewsCount = row.createCell((short)5);
						cellNewsCount.setCellValue(size);
						cellNewsCount.setCellStyle(cellStyle);
						
						cellTitle = row.createCell((short)6);
						cellTitle.setCellValue(stockNewsDataVO.getTitle());
						cellTitle.setCellStyle(cellStyle);
						
						cellDatePublished = row.createCell((short)7);
						cellDatePublished.setCellValue(stockNewsDataVO.getDatePublished().toString());
						cellDatePublished.setCellStyle(cellStyle);
						
						cellSubTitle = row.createCell((short)8);
						cellSubTitle.setCellValue(stockNewsDataVO.getSubTitle());
						cellSubTitle.setCellStyle(cellStyle);
						
						cellArticleSummary = row.createCell((short)9);
						cellArticleSummary.setCellValue(stockNewsDataVO.getArticleTextSummary());
						cellArticleSummary.setCellStyle(cellStyle);
						
						cellPortal = row.createCell((short)10);
						cellPortal.setCellValue(stockNewsDataVO.getPortal());
						cellPortal.setCellStyle(cellStyle);
						
						cellUrl = row.createCell((short)11);
						cellUrl.setCellValue(stockNewsDataVO.getUrl());
						cellUrl.setCellStyle(cellStyle);
					}
					
					// Merge cells
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,0,0)); // script name
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,1,1)); // LMP
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,2,2)); // Price Change
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,3,3)); // Percentage Change
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,4,4)); // previous close
					sheet.addMergedRegion(new CellRangeAddress(rowNum-size+1,rowNum,5,5)); // News Count
				}
				else
				{// for scripts with no news
					row = sheet.createRow(++rowNum);

					cellScriptName = row.createCell((short)0);
					cellScriptName.setCellValue(stockQuoteDataVO.getScriptName());
					cellScriptName.setCellStyle(cellStyle);
					
					cellLastMarketPrice = row.createCell((short)1);
					cellLastMarketPrice.setCellValue(stockQuoteDataVO.getLastMarketPrice());
					cellLastMarketPrice.setCellStyle(cellStyle);
					
					cellPriceChange = row.createCell((short)2);
					cellPriceChange.setCellValue(stockQuoteDataVO.getChangePrice());
					cellPriceChange.setCellStyle(cellStyle);
					
					cellPercentageChange = row.createCell((short)3);
					cellPercentageChange.setCellValue(stockQuoteDataVO.getChangePercentage());
					cellPercentageChange.setCellStyle(cellStyle);
					
					cellVolume = row.createCell((short)4);
					cellVolume.setCellValue(stockQuoteDataVO.getVoulmeTraded());
					cellVolume.setCellStyle(cellStyle);
					
					cellNewsCount = row.createCell((short)5);
					cellNewsCount.setCellValue(0);
					cellNewsCount.setCellStyle(cellStyle);
				}
			}// if
		}// While
		}
		catch(Exception e)
		{
			log.error("Exception in populating Excel");
			log.error(e);
		}
	}
	
	private void setColumnWidth(HSSFSheet sheet)
	{
		sheet.setColumnWidth(0, 3000);// script name
		sheet.setColumnWidth(1, 2000);// LMP
		sheet.setColumnWidth(2, 2000);// price change
		sheet.setColumnWidth(3, 2000);// % change
		sheet.setColumnWidth(4, 2000);// previous Close
		sheet.setColumnWidth(5, 1000);// news Count
		sheet.setColumnWidth(6, 8000);// title
		sheet.setColumnWidth(7, 3000);// date published
		sheet.setColumnWidth(8, 18000);// sub title
		sheet.setColumnWidth(9, 18000);// article summary
		sheet.setColumnWidth(10, 4000);// portal
		sheet.setColumnWidth(11, 12000);// url
	}
}
