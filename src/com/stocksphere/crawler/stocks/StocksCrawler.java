package com.stocksphere.crawler.stocks;

import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.stocksphere.common.AppConstants;
import com.stocksphere.morphia.MorphiaUtils;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class StocksCrawler extends WebCrawler {

	private static org.apache.log4j.Logger log = Logger.getLogger(StocksCrawler.class);
	
	private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4"
			+ "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	public boolean shouldVisit(Page page, WebURL url) {
		String href = url.getURL().toLowerCase();
		return !FILTERS.matcher(href).matches();
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page)
	{

		String url = page.getWebURL().getURL();

		log.info("CURRENT URL: "+ url);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

			String html = htmlParseData.getHtml();
			
			Document document = Jsoup.parseBodyFragment(html);

			if(document != null)
			{
				Elements tdElements = null;
				try
				{
					Elements trElements = document.select("table[class=tbldata14 bdrtpg] tr");

					String scriptName = null;
					String lastMarketPriceStr = null;
					String changePriceStr = null;
					String changePercentageStr = null;
					String prvClosePriceStr = null;

					double lastMarketPrice = 0;;
					double changePrice = 0;
					double changePercentage = 0;
					double prvClosePrice = 0;

					//start from second element hence i=1 - to remove headers
					for(int i=1; i<trElements.size();i++)
					{
						tdElements = trElements.get(i).getElementsByTag("td");

						if(tdElements.hasClass("brdrgtgry"))
						{
							scriptName =tdElements.get(0).text();
							lastMarketPriceStr = tdElements.get(3).text().replace(",", ""); // removing all commas(,)
							prvClosePriceStr = tdElements.get(4).text().replace(",", ""); // removing all commas(,)
							changePriceStr = tdElements.get(5).text().replace(",", ""); // removing all commas(,)
							changePercentageStr = tdElements.get(6).text().replace(",", ""); // removing all commas(,)

							lastMarketPrice = Double.parseDouble(lastMarketPriceStr);
							changePrice = Double.parseDouble(changePriceStr);
							changePercentage = Double.parseDouble(changePercentageStr); 
							prvClosePrice = Double.parseDouble(prvClosePriceStr);

							/*System.out.println("Script Name: "+scriptName);
							System.out.println("Previous Close: "+prvClosePrice);
							System.out.println("LMP: "+lastMarketPrice);
							System.out.println("Change: "+changePrice);
							System.out.println("% Change: "+changePercentage);
							System.out.println("___________________________________________________________________________");*/

							// insert to mongoDB
							new MorphiaUtils().insertStockQuoteDetails(scriptName, lastMarketPrice, changePrice, changePercentage, prvClosePrice, AppConstants.STOCK_NEWS_PORTAL_MONEYCONTROL);
						}
					}
				}
				catch(Exception e)
				{
					log.error("Exception in parsing for URL: "+ url);
					log.error(e);
				}
			}
		}
	}
}