package com.stocksphere.crawler.news;

import org.apache.log4j.Logger;

import com.stocksphere.common.AppConstants;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;


public class NewsBot {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(NewsBot.class);

	public static void main(String[] args) {
		
		/*
	     * crawlStorageFolder is a folder where intermediate crawl data is
	     * stored.
	     */
	    String crawlStorageFolder = AppConstants.USER_PATH+AppConstants.NEWS_CRAWL_STORAGE_FOLDER;

	    /*
	     * numberOfCrawlers shows the number of concurrent threads that should
	     * be initiated for crawling.
	     */
	    int numberOfCrawlers = Integer.parseInt(AppConstants.NEWS_NUMBER_OF_CRAWLERS);

	    CrawlConfig config = new CrawlConfig();

	    config.setCrawlStorageFolder(crawlStorageFolder);

	    /*
	     * Be polite: Make sure that we don't send more than 1 request per
	     * second (1000 milliseconds between requests).
	     */
	    config.setPolitenessDelay(1500);

	    /*
	     * You can set the maximum crawl depth here. The default value is -1 for
	     * unlimited depth
	     */
	    config.setMaxDepthOfCrawling(Integer.parseInt(AppConstants.NEWS_DEPTH_OF_CRAWLING));

	    /*
	     * You can set the maximum number of pages to crawl. The default value
	     * is -1 for unlimited number of pages
	     */
	    config.setMaxPagesToFetch(100000);
	    
	    // user agent
	    config.setUserAgentString(AppConstants.CRAWLER_USER_AGENT);

	    /*
	     * Do you need to set a proxy? If so, you can use:
	     * config.setProxyHost("proxyserver.example.com");
	     * config.setProxyPort(8080);
	     *
	     * If your proxy also needs authentication:
	     * config.setProxyUsername(username); config.getProxyPassword(password);
	     */

	    /*
	     * This config parameter can be used to set your crawl to be resumable
	     * (meaning that you can resume the crawl from a previously
	     * interrupted/crashed crawl). Note: if you enable resuming feature and
	     * want to start a fresh crawl, you need to delete the contents of
	     * rootFolder manually.
	     */
	    config.setResumableCrawling(false);

	    /*
	     * Instantiate the controller for this crawl.
	     */
	    PageFetcher pageFetcher = new PageFetcher(config);
	    RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
	    RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
	    CrawlController controller;
		try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
		

	    /*
	     * For each crawl, you need to add some seed urls. These are the first
	     * URLs that are fetched and then the crawler starts following links
	     * which are found in these pages
	     */
			controller.addSeed("http://www.moneycontrol.com/news/");
			controller.addSeed("http://economictimes.indiatimes.com/");
			controller.addSeed("http://www.moneycontrol.com/stocksmarketsindia/");
			controller.addSeed("http://economictimes.indiatimes.com/markets");
			controller.addSeed("http://economictimes.indiatimes.com/news");
			controller.addSeed("http://www.moneycontrol.com/earnings/");
			controller.addSeed("http://www.moneycontrol.com/stock-advice/");
			controller.addSeed("http://www.moneycontrol.com/ipo/");
			
			controller.addSeed("http://economictimes.indiatimes.com/industry");
			controller.addSeed("http://economictimes.indiatimes.com/wealth");
			controller.addSeed("http://economictimes.indiatimes.com/mutual-funds");
			controller.addSeed("http://economictimes.indiatimes.com/tech");
			controller.addSeed("http://economictimes.indiatimes.com/opinion");
			controller.addSeed("http://economictimes.indiatimes.com/markets/stocks/news/articlelist/2146843.cms");
			controller.addSeed("http://economictimes.indiatimes.com/markets/stocks/announcements/articlelist/2165029.cms");
			controller.addSeed("http://economictimes.indiatimes.com/markets/stocks/earnings/articlelist/5766568.cms");
			controller.addSeed("http://economictimes.indiatimes.com/markets/stocks/recos/articlelist/3053611.cms");
			controller.addSeed("http://economictimes.indiatimes.com/markets/stocks/policy/articlelist/1205979708.cms");
			controller.addSeed("http://economictimes.indiatimes.com/markets/ipos/fpos/rights-issues");
			
			log.info("Starting crawl");
		    
	    /*
	     * Start the crawl. This is a blocking operation, meaning that your code
	     * will reach the line after this only when crawling is finished.
	     */
	    controller.start(NewsCrawler.class, numberOfCrawlers);
	    
		} catch (Exception e) {
			log.error("Exception in NewsBot");
			log.error(e);
		}
	  }
}
