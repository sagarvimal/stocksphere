package com.stocksphere.crawler.news;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.stocksphere.common.AppConstants;
import com.stocksphere.morphia.MorphiaUtils;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class NewsCrawler extends WebCrawler {

	private static org.apache.log4j.Logger log = Logger.getLogger(NewsCrawler.class);

	private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4"
			+ "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	public boolean shouldVisit(Page page, WebURL url) {
		String href = url.getURL().toLowerCase();

		return !FILTERS.matcher(href).matches();
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page)
	{

		String url = page.getWebURL().getURL();
		String domain = page.getWebURL().getDomain();

		log.info("CURRENT URL: "+ url);
		log.info("DOMAIN: "+ domain);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

			String html = htmlParseData.getHtml();

			Document document = Jsoup.parseBodyFragment(html);

			if(document != null)
			{
				String date = null;
				String source = null;
				String title = null;
				String subTitle = null;
				String articleText = null;
				Date datePublished = null;

				Elements dateSourceElements = null;
				Elements titleElements = null;
				Elements subtitleElements = null;
				Elements articleTextElements = null;

				try
				{
					if(domain != null && domain.equalsIgnoreCase(AppConstants.DOMAIN_MONEYCONTROL))
					{
						// date and source
						dateSourceElements = document.select("div[class=tpIcnDt clearfix]");
						// title
						titleElements = document.select("h1[class=mTitle]");
						// subtitle
						subtitleElements = document.select("p[class=subTitle brdb PT10");
						//article text
						articleTextElements = document.select("div[id=div_art][class=artclText]");

						if(dateSourceElements != null && !dateSourceElements.isEmpty() && titleElements != null && !titleElements.isEmpty()
								&& subtitleElements != null && !subtitleElements.isEmpty() && articleTextElements != null && !articleTextElements.isEmpty())
						{
							log.info("Processing URL");

							String[] tempStr = dateSourceElements.select("div[class=FL]").text().toString().split("\\|");
							date = tempStr[0];
							source = dateSourceElements.select("span[class=op_bl11]").text();

							title = titleElements.text();
							subTitle = subtitleElements.text();
							articleText = articleTextElements.text();
							datePublished = new SimpleDateFormat(AppConstants.MONEYCONTROL_DATE_FORMAT).parse(date);

							DateTime dateTimePublished = null;
							Duration duration = null;
							DateTime dateTimeNow = new DateTime(new Date()); 

							dateTimePublished = new DateTime(datePublished);
							duration = new Duration(dateTimePublished,dateTimeNow);
							// News latest to 100 hours is allowed
							if(duration != null && Math.abs(duration.getStandardHours()) <= AppConstants.STANDARD_HOURS)
							{
								// insert to Mongo DB
								new MorphiaUtils().insertNewsData(title, subTitle, datePublished, source, url, articleText, AppConstants.STOCK_NEWS_PORTAL_MONEYCONTROL);
							}
							else
							{
								log.info("Old News! Publised before 100 hours. Date Published: "+ datePublished.toString());
							}
						}
						else
						{
							log.info("This is not a News URL!");
						}
					}
					else if(domain != null && domain.equalsIgnoreCase(AppConstants.DOMAIN_ET))
					{
						// date and source
						dateSourceElements = document.select("div[class=byline]");
						// title
						titleElements = document.select("h1[class=title]");
						// subtitle
						subtitleElements = document.select("div[class=articleImg]").tagName("figure").tagName("figcaption");
						//article text
						articleTextElements = document.select("div[class=artText]");

						if(dateSourceElements != null && !dateSourceElements.isEmpty() && titleElements != null && !titleElements.isEmpty()
								&& subtitleElements != null && !subtitleElements.isEmpty() && articleTextElements != null && !articleTextElements.isEmpty())
						{
							log.info("Processing URL");

							if(dateSourceElements.text().toString().contains("|"))
							{
								String[] tempStr = dateSourceElements.text().toString().split("\\|");
								date = tempStr[1];
								source = tempStr[0];

								if(source.startsWith("By"))
								{
									source = source.substring(2);
								}
							}
							else
							{
								date = dateSourceElements.text().toString();
								source = "ET";
							}

							title = titleElements.text();
							subTitle = subtitleElements.text();
							articleText = articleTextElements.text();
							datePublished = new SimpleDateFormat(AppConstants.ET_DATE_FORMAT).parse(date);

							DateTime dateTimePublished = null;
							Duration duration = null;
							DateTime dateTimeNow = new DateTime(new Date()); 

							dateTimePublished = new DateTime(datePublished);
							duration = new Duration(dateTimePublished,dateTimeNow);
							// News latest to 100 hours is allowed
							if(duration != null && Math.abs(duration.getStandardHours()) <= AppConstants.STANDARD_HOURS)
							{
								// insert to Mongo DB
								new MorphiaUtils().insertNewsData(title, subTitle, datePublished, source, url, articleText, AppConstants.STOCK_NEWS_PORTAL_ET);
							}
							else
							{
								log.info("Old News! Publised before 100 hours. Date Published: "+ datePublished.toString());
							}
						}
						else
						{
							log.info("This is not a News URL!");
						}
					}
					else
					{
						log.info("This is not a valid domain: "+ domain);
					}
				}
				catch(Exception e)
				{
					log.error("Exception in parsing for URL: "+ url);
					log.error(e);
				}
			} 
		}
	}
}