package com.stocksphere.datavalueobject;

import java.util.Date;
import java.util.List;

public class StockQuoteDataVO {

	private String scriptName; 
	
	private double lastMarketPrice;
	
	private double changePrice;
	
	private double changePercentage;
	
	private double voulmeTraded;
	
	private String portal;
	
	private Date createdDate;
	
	private String createdBy;
	
	private List<StockNewsDataVO> StockNewsDataVOList;

	public String getScriptName() {
		return scriptName;
	}

	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	public double getLastMarketPrice() {
		return lastMarketPrice;
	}

	public void setLastMarketPrice(double lastMarketPrice) {
		this.lastMarketPrice = lastMarketPrice;
	}

	public double getChangePrice() {
		return changePrice;
	}

	public void setChangePrice(double changePrice) {
		this.changePrice = changePrice;
	}

	public double getChangePercentage() {
		return changePercentage;
	}

	public void setChangePercentage(double changePercentage) {
		this.changePercentage = changePercentage;
	}

	public double getVoulmeTraded() {
		return voulmeTraded;
	}

	public void setVoulmeTraded(double voulmeTraded) {
		this.voulmeTraded = voulmeTraded;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<StockNewsDataVO> getStockNewsDataVOList() {
		return StockNewsDataVOList;
	}

	public void setStockNewsDataVOList(List<StockNewsDataVO> stockNewsDataVOList) {
		StockNewsDataVOList = stockNewsDataVOList;
	}
}
