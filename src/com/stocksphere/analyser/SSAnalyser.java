package com.stocksphere.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.stocksphere.common.AppConstants;
import com.stocksphere.common.ExcelUtils;
import com.stocksphere.common.SSComparator;
import com.stocksphere.datavalueobject.StockNewsDataVO;
import com.stocksphere.datavalueobject.StockQuoteDataVO;
import com.stocksphere.morphia.MorphiaUtils;
import com.stocksphere.morphia.valueobjects.StockNewsVO;
import com.stocksphere.morphia.valueobjects.StockQuoteVO;


public class SSAnalyser {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(SSAnalyser.class);
	
	private static List<StockQuoteDataVO> scriptWithNoNewsList = new LinkedList<StockQuoteDataVO>();

	public static void main(String[] args) {

		log.info("Cheking....");
		try
		{
			MorphiaUtils morphiaUtils = new MorphiaUtils();

			//<20
			List<StockQuoteVO> category1 = null;
			//>20 - <60
			List<StockQuoteVO> category2 = null;
			//>60 - 150
			List<StockQuoteVO> category3 = null;
			//>150 - <350
			List<StockQuoteVO> category4 = null;
			//>350
			List<StockQuoteVO> category5 = null;

			List<StockQuoteVO> stockQuoteVOList = morphiaUtils.getAllDocumentsOfCollection(StockQuoteVO.class);

			if(stockQuoteVOList != null && !stockQuoteVOList.isEmpty())
			{
				//Sort List with percentage
				SSComparator.COMPARISION_PARAMETER = AppConstants.COMPARISION_PARAMETER_PERCENTAGE;
				Collections.sort(stockQuoteVOList,new SSComparator());

				Iterator<StockQuoteVO> itrq = stockQuoteVOList.iterator();
				
				StockQuoteVO stockQuoteVO = null;

				while(itrq.hasNext())
				{
					 stockQuoteVO = itrq.next();
					 
					 if(stockQuoteVO.getLastMarketPrice() <= 20)
					 {
						 if(category1 == null)
						 {
							 category1 = new ArrayList<StockQuoteVO>();
						 }
						 category1.add(stockQuoteVO);
					 }
					 else if(stockQuoteVO.getLastMarketPrice() > 20 && stockQuoteVO.getLastMarketPrice() <= 60)
					 {
						 if(category2 == null)
						 {
							 category2 = new ArrayList<StockQuoteVO>();
						 }
						 category2.add(stockQuoteVO);
					 }
					 else if(stockQuoteVO.getLastMarketPrice() > 60 && stockQuoteVO.getLastMarketPrice() <= 150)
					 {
						 if(category3 == null)
						 {
							 category3 = new ArrayList<StockQuoteVO>();
						 }
						 category3.add(stockQuoteVO);
					 }
					 else if(stockQuoteVO.getLastMarketPrice() > 150 && stockQuoteVO.getLastMarketPrice() <= 350)
					 {
						 if(category4 == null)
						 {
							 category4 = new ArrayList<StockQuoteVO>();
						 }
						 category4.add(stockQuoteVO);
					 }
					 else if(stockQuoteVO.getLastMarketPrice() > 350)
					 {
						 if(category5 == null)
						 {
							 category5 = new ArrayList<StockQuoteVO>();
						 }
						 category5.add(stockQuoteVO);
					 }
				}//while
				
				// Create map of structured data
				SSAnalyser sSAnalyser = new SSAnalyser();
				LinkedHashMap<String,List<StockQuoteDataVO>> linkedHashMap = new LinkedHashMap<>();
				
				//Creating Map for All stocks
				log.info("creating map for all stocks");
				linkedHashMap.put(AppConstants.CATEGORY_ALL, sSAnalyser.filterdata(stockQuoteVOList));
				
				// Filter and print details
				if(category1 != null && !category1.isEmpty())
				{
					log.info("creating map for stocks <=20");
					linkedHashMap.put(AppConstants.CATEGORY_1, sSAnalyser.filterdata(category1));
				}
				if(category2 != null && !category2.isEmpty())
				{
					log.info("creating map for stocks >20 && <=60");
					linkedHashMap.put(AppConstants.CATEGORY_2, sSAnalyser.filterdata(category2));
				}
				if(category3 != null && !category3.isEmpty())
				{
					log.info("creating map for stocks >60 && <=150");
					linkedHashMap.put(AppConstants.CATEGORY_3, sSAnalyser.filterdata(category3));
				}
				if(category4 != null && !category4.isEmpty())
				{
					log.info("creating map for stocks >150 && <=350");
					linkedHashMap.put(AppConstants.CATEGORY_4, sSAnalyser.filterdata(category4));
				}
				if(category5 != null && !category5.isEmpty())
				{
					log.info("creating map for stocks >350");
					linkedHashMap.put(AppConstants.CATEGORY_5, sSAnalyser.filterdata(category5));
				}
				
				// print data in excel
				new ExcelUtils().printDataTOExcel(linkedHashMap,scriptWithNoNewsList);
			}
		}
		catch(Exception e)
		{
			log.error(e);
		}

		log.info("----- End of execution -----");
	}

	private List<StockQuoteDataVO> createPrintableData(LinkedHashMap<String, StockQuoteVO> quoteMap,HashMap<String, List<StockNewsVO>> newsMap)
	{
		log.info("Inside createPrintableData()");
		
		StockNewsDataVO stockNewsDataVO = null;
		StockQuoteDataVO stockQuoteDataVO = null;
		List<StockQuoteDataVO> stockQuoteDataVOList = null;
		
		if(quoteMap != null && !quoteMap.isEmpty())
		{
			try
			{
				Iterator<Entry<String, StockQuoteVO>> it = quoteMap.entrySet().iterator();

				StockQuoteVO stockQuoteVO = null;
				StockNewsVO stockNewsVO = null;
				List<StockNewsVO> stockNewsVOList = null;

				stockQuoteDataVOList = new ArrayList<StockQuoteDataVO>();
				while (it.hasNext()) 
				{
					Entry<String, StockQuoteVO> keyValuePairs = it.next();

					String key = keyValuePairs.getKey();

					stockQuoteVO = quoteMap.get(key);
					stockNewsVOList = newsMap.get(key);

					stockQuoteDataVO = new StockQuoteDataVO();
					stockQuoteDataVO.setScriptName(stockQuoteVO.getScriptName());
					stockQuoteDataVO.setLastMarketPrice(stockQuoteVO.getLastMarketPrice());
					stockQuoteDataVO.setChangePrice(stockQuoteVO.getChangePrice());
					stockQuoteDataVO.setChangePercentage(stockQuoteVO.getChangePercentage());
					stockQuoteDataVO.setVoulmeTraded(stockQuoteVO.getVoulmeTraded());

					if(stockNewsVOList != null && !stockNewsVOList.isEmpty())
					{
						List<StockNewsDataVO> stcokNewsDataVOList = new ArrayList<StockNewsDataVO>();
						for(int i=0; i< stockNewsVOList.size(); i++)
						{
							stockNewsVO = stockNewsVOList.get(i);

								stockNewsDataVO = new StockNewsDataVO();
								stockNewsDataVO.setTitle(stockNewsVO.getTitle());
								stockNewsDataVO.setSubTitle(stockNewsVO.getSubTitle());
								stockNewsDataVO.setDatePublished(stockNewsVO.getDatePublished());
								stockNewsDataVO.setPortal(stockNewsVO.getPortal());
								stockNewsDataVO.setUrl(stockNewsVO.getUrl());

								String articleText = stockNewsVO.getArticleText();

								int index = articleText.toLowerCase().indexOf(AppConstants.SPACE+stockQuoteVO.getScriptName().toLowerCase()+AppConstants.SPACE);
								if(index != -1)
								{
									int start = 0;
									int end = articleText.length()-1;
									int total = 100;

									if(index -total > start)
									{
										start = index - total;
									}

									if(index + total < end)
									{
										end = index + total;
									}
									stockNewsDataVO.setArticleTextSummary(AppConstants.DOTS+articleText.substring(start,end)+AppConstants.DOTS);

								}// if 
								stcokNewsDataVOList.add(stockNewsDataVO);	
						}// for 
						
						if(stcokNewsDataVOList != null && !stcokNewsDataVOList.isEmpty())
						{
							// sort news list with date published
							SSComparator.COMPARISION_PARAMETER = AppConstants.COMPARISION_PARAMETER_DATE_PUBLISHED;
							Collections.sort(stcokNewsDataVOList,new SSComparator());
							stockQuoteDataVO.setStockNewsDataVOList(stcokNewsDataVOList);
						}
					}
					stockQuoteDataVOList.add(stockQuoteDataVO);
				}// while
			}// try
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}// if quoteMap != null && !quoteMap.isEmpty()
		
		log.info("Exiting createPrintableData()");
		return stockQuoteDataVOList;
	}

	private List<StockQuoteDataVO> filterdata(List<StockQuoteVO> stockQuoteVOList)
	{
		log.info("Inside filterdata()");
		
		MorphiaUtils morphiaUtils = new MorphiaUtils();
		List<StockQuoteDataVO> stockQuoteDataVOList = null;

		LinkedHashMap<String, StockQuoteVO> quoteMap =  new LinkedHashMap<String, StockQuoteVO>();
		HashMap<String, List<StockNewsVO>> newsMap = new HashMap<String, List<StockNewsVO>>();
		DateTime dateTimeNow = new DateTime(new Date());

		if(stockQuoteVOList != null && !stockQuoteVOList.isEmpty())
		{
			//Sort List with percentage
			Collections.sort(stockQuoteVOList,new SSComparator());

			Iterator<StockQuoteVO> itrq = stockQuoteVOList.iterator();

			while(itrq.hasNext())
			{
				StockQuoteVO stockQuoteVO = itrq.next();
				
				List<StockNewsVO> stockNewsVOList = morphiaUtils.getAllDocumentsOfCollection(StockNewsVO.class);

				List<StockNewsVO> matchedNewVOList = null;

				if(stockNewsVOList != null && !stockNewsVOList.isEmpty())
				{
					Iterator<StockNewsVO> itrn = stockNewsVOList.iterator();

					while(itrn.hasNext())
					{
						StockNewsVO stockNewsVO = itrn.next();

						if(stockNewsVO.getTitle().toLowerCase().contains(AppConstants.SPACE+stockQuoteVO.getScriptName().toLowerCase()+AppConstants.SPACE) || 
								stockNewsVO.getSubTitle().toLowerCase().contains(AppConstants.SPACE+stockQuoteVO.getScriptName().toLowerCase()+AppConstants.SPACE) || 
								stockNewsVO.getArticleText().toLowerCase().contains(AppConstants.SPACE+stockQuoteVO.getScriptName().toLowerCase()+AppConstants.SPACE))
						{
							DateTime dateTimePublished = null;
							Duration duration = null;
							if(stockNewsVO.getDatePublished() != null)
							{
								dateTimePublished = new DateTime(stockNewsVO.getDatePublished());
								duration = new Duration(dateTimePublished,dateTimeNow);
							}
							// News latest to 100 hours is allowed
							if(duration != null && Math.abs(duration.getStandardHours()) <= AppConstants.STANDARD_HOURS)
							{
								if(matchedNewVOList == null)
								{
									matchedNewVOList = new ArrayList<StockNewsVO>();
								}

								matchedNewVOList.add(stockNewsVO);
							}
						}
					}

					if(matchedNewVOList != null && !matchedNewVOList.isEmpty())
					{
						quoteMap.put(stockQuoteVO.getScriptName(), stockQuoteVO);
						newsMap.put(stockQuoteVO.getScriptName(), matchedNewVOList);
					}
					else
					{
						if(scriptWithNoNewsList != null && scriptWithNoNewsList.size() < 30)
						{
							// added in the list of script with no news
							StockQuoteDataVO quoteDataVO = new StockQuoteDataVO();
							quoteDataVO.setScriptName(stockQuoteVO.getScriptName());
							quoteDataVO.setChangePercentage(stockQuoteVO.getChangePercentage());
							quoteDataVO.setChangePrice(stockQuoteVO.getChangePrice());
							quoteDataVO.setLastMarketPrice(stockQuoteVO.getLastMarketPrice());
							quoteDataVO.setVoulmeTraded(stockQuoteVO.getVoulmeTraded());

							scriptWithNoNewsList.add(quoteDataVO);
						}
					}
				}
			}
			if(!quoteMap.isEmpty() && !newsMap.isEmpty())
			{
				stockQuoteDataVOList = this.createPrintableData(quoteMap, newsMap);
			}
		}// if
		
		log.info("Exiting filterdata()");
		 return stockQuoteDataVOList;
	}
}
