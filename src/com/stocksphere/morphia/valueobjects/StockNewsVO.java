package com.stocksphere.morphia.valueobjects;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import com.stocksphere.common.AppConstants;

@Entity(value="db_col_stock_news",noClassnameStored = true)
public class StockNewsVO {
	
	public StockNewsVO() {
		
		this.createdBy = AppConstants.BOT_NAME;
		
		this.createdDate = new Date();
	}
	
	@Id
	@Property("_id")
	private ObjectId id;
	
	@Property("title")
	private String title; 
	
	@Property("subTitle")
	private String subTitle;
	
	@Property("date_published")
	private Date datePublished;
	
	@Property("source")
	private String source;
	
	@Property("url")
	private String url;
	
	@Property("article_text")
	private String articleText;
	
	@Property("portal")
	private String portal;
	
	@Property("created_date")
	private Date createdDate;
	
	@Property("created_by")
	private String createdBy;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Date getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getArticleText() {
		return articleText;
	}

	public void setArticleText(String articleText) {
		this.articleText = articleText;
	}

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
