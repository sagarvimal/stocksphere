package com.stocksphere.morphia;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.stocksphere.common.AppConstants;
import com.stocksphere.morphia.valueobjects.StockNewsVO;
import com.stocksphere.morphia.valueobjects.StockQuoteVO;

/*
 * https://github.com/mongodb/morphia/wiki/Query
 */
public class MorphiaUtils {
	
	private static org.apache.log4j.Logger log = Logger.getLogger(MorphiaUtils.class);
	
	private static MongoClient mongoClient = null;
	
	public MorphiaUtils()
	{
		if(mongoClient == null)
		{
			try 
			{
				mongoClient = new MongoClient(AppConstants.MONGO_DB_CONNECTION);
			} catch (Exception e) 
			{
				log.error(e);
			}
		}
	}
	
	public void insertNewsData(String title, String subTitle, Date datePublished, String source, String url, String articleText,String portal)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			// query to check if entry already exists
			Query<StockNewsVO> query = ds.createQuery(StockNewsVO.class).filter(AppConstants.NEWS_URL, url);

			StockNewsVO stockNewsVO = null;

			stockNewsVO = query.get();

			if(stockNewsVO != null && stockNewsVO.getUrl().equalsIgnoreCase(url))
			{
				log.info("Document alreday present for URL: "+ url);
			}
			else
			{
				stockNewsVO = new StockNewsVO();

				stockNewsVO.setTitle(title);
				stockNewsVO.setSubTitle(subTitle);
				stockNewsVO.setDatePublished(datePublished);
				stockNewsVO.setSource(source);
				stockNewsVO.setUrl(url);
				stockNewsVO.setArticleText(articleText);
				stockNewsVO.setPortal(portal);

				ds.save(stockNewsVO);
				
				log.info("Document inserted successfully for URL: "+ url);
			}
		}
		catch(Exception e)
		{
			log.error("Exception in inserting document for URL: "+ url);
			
			log.error(e);
		}
	}
	
	public void insertStockQuoteDetails(String scriptName, double lastMarketPrice, double changePrice, double changePercentage, double voulmeTraded, String portal)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

			// query to check if entry already exists
			Query<StockQuoteVO> query = ds.createQuery(StockQuoteVO.class).filter(AppConstants.SCRIPT_NAME, scriptName);

			StockQuoteVO stockQuoteVO = null;

			stockQuoteVO = query.get();
			
			if(stockQuoteVO != null && stockQuoteVO.getScriptName().equalsIgnoreCase(scriptName))
			{
				log.info("Document alreday present for script: "+ scriptName);
			}
			else
			{
				stockQuoteVO = new StockQuoteVO();
				
				stockQuoteVO.setScriptName(scriptName);
				stockQuoteVO.setLastMarketPrice(lastMarketPrice);
				stockQuoteVO.setChangePrice(changePrice);
				stockQuoteVO.setChangePercentage(changePercentage);
				stockQuoteVO.setVoulmeTraded(voulmeTraded);
				stockQuoteVO.setPortal(portal);

				ds.save(stockQuoteVO);
				
				log.info("Document inserted successfully for Script: "+ scriptName);
			}
		}
		catch(Exception e)
		{
			log.error("Exception in inserting document for Script: "+ scriptName);
			log.error(e);
		}
	}
	
	public void dropCollection(Class<?> collectionClass)
	{
		try
		{
			Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);
			
			DBCollection dbCollection = ds.getCollection(collectionClass);
			
			if (dbCollection != null)
			{
				dbCollection.drop();
				log.info("Collection successfully removed : "+ dbCollection.getFullName());
			}
			else
			{
				log.info("Collection does not exists");
			}
		}
		catch(Exception e)
		{
			log.error(e);
		}
	}
	
	public DBCollection getCollection(String collectionName)
	{
		DBCollection dbCollection = null;
		
		try
		{
			DB db = mongoClient.getDB(AppConstants.DB_NAME);
			
			if (db.collectionExists(collectionName))
			{
				dbCollection = db.getCollection(collectionName);
			}
			else
			{
				log.info("Collection does not exists : "+ collectionName);
			}
		}
		catch(Exception e)
		{
			log.error(e);
		}
		
		return dbCollection;
	}
	
	public <T> List<T> getAllDocumentsOfCollection(Class<T> classsName)
	{
		Datastore ds = new Morphia().createDatastore(mongoClient,AppConstants.DB_NAME);

		// query to check if entry already exists
		Query<T> query = ds.createQuery(classsName);

		return (List<T>) query.asList();
	}
	
}
