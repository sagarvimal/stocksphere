import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


public class Test {
	
	public static void main(String[] args) {
		
		try 
		{
			DateTime dateTime = new DateTime(new Date()).withZone(DateTimeZone.forID( "Asia/Kolkata" ));
			
			System.out.println(dateTime.toDate());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}